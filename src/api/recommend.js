import jsonp from 'common/js/jsonp'
import {commonParams, options} from "./config";
import axios from 'axios'
export function getRecommend() {

  const url = 'https://c.y.qq.com/musichall/fcgi-bin/fcg_yqqhomepagerecommend.fcg';

  const data =Object.assign({},commonParams,{
    platform: 'h5',
    uin: 0,
    needNewCode: 1
  })

  return jsonp(url,data,options)
}

export function getDiscList() {
  // const url = '/api/getDiscList'
  const url = '/static/json/disclist.json';
  const data = Object.assign({}, commonParams,{
    format: 'json'
  })

  return axios.get(url, {
    params: data
  }).then((res) => {
    return Promise.resolve(res.data)
  })
  // return jsonp(url,data,options)
}
